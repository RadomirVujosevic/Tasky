
# Running

git clone https://gitlab.com/RadomirVujosevic/Tasky.git  
cd Tasky  
npm i  

Make a new database and configure connection options in the .env file. Check *env file* section

npm run migration:run  
This will run the SetupDB migration which sets up the tables and rules for db relations  

npm run start

### Optional:  
Send one GET request to http:/localhost:3000/seed this will trigger the seed() function in app service which populates the DB with fake data  
Very important because the initial database has no roles(Admin manager employee) and it has no administrator. The api doesn't provide a way to create these  

Alternatively you could insert the 3 roles into the table and create fake data on your own  

# Authentication and Authorization

The first 4 requests in the *Api reference* section have no security.  
The login path returns a Bearer jwt_token which is required in ALL other requests.  
The token contains data about the user 

Authorization is role based  
The *Api reference* section is split based on the roles  
Check controller files for details @Roles() decorator serves to restrict access based on roles.  
If there is no decorator, its not protected by roles - its meant for employees, but all other roles can use it too. 

## env file
.env file should contain the following values:  
JWT_SECRET: *secret key string*  
DB_HOST: *host url* (default "localhost")
DB_USERNAME: *db connection username* (default "postgres")
DB_PASSWORD: *db connection password* (default "")
DB_NAME: *database name*


# Api reference

A list of the available request paths. Check the controller files for more details  

## Should not be used in production. Only useful for bugging
Get(/seed) populates the database with initial data  
Delete(/unseed) deletes the entire database  

Post(/login) attempts to login. Returns jwt token on success  
Post(/register) creates a new user. Check create-user.dto.ts for more details.  

## Employee 

### Employees are allowed to edit tasks if they are assigned to the user or they belong to a project the user is working on 
For example, user A works on projects X and Y. User B works only on project X. User B can view modify only tasks assigned to user A that are part of project X  

Get(/task) returns information about tasks  
Get(/task/id/:id) returns information about a specific task based on id. 
Post(/task) Creates a new task. Performs validation on the request body. Check create-task.dto.ts for details  
Delete(/task/id/:id) deletes a specific task.  
Patch(/task/id/:id) update a specific task.  


Get(/users/tasks) returns all tasks assigned to the user  
Get(/users/projects) returns all projects the user is working  
Get(/users/id/:id) returns data about the user based on id. Users are only allowed to access their data or the data of other users working on shared projects  

# Manager

### Manager is able to send all requests that the employee can send, along with the ones listed below.

Patch(/project/id/:id) Updates the project. Only possible if the user is acually assigned to the project  


# Admin

Get(/users) Returns a full list of users  
Get(/task) Returns a full list of tasks  
Get(/project) Returns a full list of projects  

Get(/users/id/:id) Returns detail about user based on id  
Delete(/users/id/:id) Deletes user based on id  
Patch(/users/id/id) Updates user based on id. Check user-update.dto.ts for details 

Get(/task/id/:id) Returns detail about user based on id  
Delete(/users/id/:id) Deletes user based on id  
Patch(/users/id/id) Updates user based on id. Check user-update.dto.ts for details  

Get(/task/id/:id) returns information about a specific task based on id  
Post(/task) Creates a new task. Check create-task.dto.ts for details   
Delete(/task/id/:id) deletes a specific task.  
Patch(/task/id/:id) update a specific task.  

Get(/project/id/:id) returns information about a specific project based on id  
Post(/project) Creates a new user. Check create-project.dto.ts for details   
Delete(/project/id/:id) deletes a specific project.  
Patch(/project/id/:id) update a specific project.  

Get(/project/id/:id/tasks) returns all tasks that are part of the project by id  
Get(/project/id/:id/tasks/byuser/:user) returns all tasks that are assigned to :user. User can be a single value or a comma seperated list  
Get(/project/id/:id/tasks/bycompletion/:complete) returns tasks based on completion status. Complete can be true or false  
Get(/project/id/:id/tasks/title/:title) returns tasks based on title
Get(/project/id/:id/tasks/sortby/priority/high) returns a sorted list of tasks based on priority. The order is high > medium > low  
Get(/project/id/:id/tasks/sortby/priority/low) returns a sorted list of tasks based on priority. The order is low > medium > high  
