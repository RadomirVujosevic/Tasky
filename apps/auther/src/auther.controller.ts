import { Controller, Get, UseGuards } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { AuthGuard } from '@nestjs/passport';
import { AutherService } from './auther.service';
import { GoogleUserData } from './entities/googleUserData';

@Controller()
export class AutherController {
  constructor(private readonly autherService: AutherService) { }

  @MessagePattern('googleLogin')
  googleUserLogin(data: GoogleUserData) {
    return this.autherService.googleLogin(data)
  }
}
