import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AutherModule } from './auther.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(AutherModule,
    {
      transport: Transport.REDIS,
      options: {
        host: 'localhost',
        port: 6379,
      }
    });
  await app.listen();
}
bootstrap();
