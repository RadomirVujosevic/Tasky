import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import e from 'express';
import { Repository } from 'typeorm';
import { GoogleUserData } from './entities/googleUserData';
import { Role } from './entities/role.entity';
import { RoleEnum } from './entities/role.enum';
import { User } from './entities/users.entity';

@Injectable()
export class AutherService {
  defaultRelations = ['role', 'worksOn'];
  constructor(
    private jwtService: JwtService,
    @InjectRepository(User) private usersRepo: Repository<User>,
    @InjectRepository(Role) private roleRepo: Repository<Role>

  ) { }

  async googleLogin(data: GoogleUserData) {

    const users = await this.usersRepo.find({
      where: {
        email: data.email
      },
      relations: this.defaultRelations,
    })
    let user: User;

    if (users.length > 0) {
      user = users.at(0);
    } else {

      const employeeRole = await this.roleRepo.findOne({
        where: { name: RoleEnum.Employee }
      });
      user = this.usersRepo.create({
        email: data.email,
        first_name: data.firstName,
        last_name: data.lastName,
        gender: "Male",
        password: "auto generated password",

      })
      user.role = employeeRole;
      user.worksOn = [];

      console.log(user);
      user = (await this.usersRepo.save(user));

    }
    console.log(user);

    const payload = {
      id: user.id,
      role: user.role.name,
      email: user.email
    }

    return this.jwtService.sign(payload);

  }
}
