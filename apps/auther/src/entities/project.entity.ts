import { User } from "./users.entity";
import { Column, Entity, JoinColumn, ManyToMany, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Project {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => User, (user) => user.id, { nullable: true, onDelete: "SET NULL", })
    manager?: User;

    @ManyToMany(() => User, (user) => user.worksOn)
    assignedUsers: User[]

    @Column({
        type: 'text',
        unique: true
    })
    name: string;
}
