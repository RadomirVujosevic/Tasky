import { User } from "./users.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { RoleEnum } from "./role.enum";

@Entity()
export class Role {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'enum', enum: RoleEnum
    })
    name: RoleEnum;

    @OneToMany(() => User, (user) => user.id)
    users: User[]
}