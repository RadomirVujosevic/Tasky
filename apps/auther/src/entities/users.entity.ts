import { Role } from "./role.entity";
import { Project } from "./project.entity";
import { Task } from "./task.entity";
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Exclude } from "class-transformer";

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length: 250,
        nullable: false
    })
    email: string;

    @Column({
        length: 250,
        nullable: false
    })
    first_name: string;

    @Column({
        length: 250,
        nullable: false
    })
    last_name: string;

    @Column({
        length: 10,
        nullable: false
    })
    gender: string;

    @Column({
        length: 50,
        nullable: false
    })
    @Exclude()
    password: string;

    @Column({
        length: 10,
        nullable: true,
    })
    phone: string | null;

    @ManyToOne(() => Role, { onDelete: 'SET NULL' })
    role: Role;

    @ManyToMany(() => Project, (project) => project.assignedUsers, {
        cascade: true
    })
    @JoinTable()
    worksOn: Project[];

    @OneToMany(() => Task, (task) => task.id)
    tasks: Task[];

    @OneToMany(() => Task, (task) => task.id)
    managedProjects: Project[];

    @OneToMany(() => Task, (task) => task.id)
    createdTasks: Task[];
}