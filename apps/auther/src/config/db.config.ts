import { ConfigService } from "@nestjs/config";
import { TypeOrmModuleAsyncOptions } from "@nestjs/typeorm";
import { DataSource, DataSourceOptions } from "typeorm";
import appConfig from "./app.config";


export const dbConfig: TypeOrmModuleAsyncOptions = {
    useFactory: async (config: ConfigService) => {

        return {
            type: "postgres",
            host: process.env.DB_HOST || "localhost",
            username: process.env.DB_USERNAME || "postgres",
            password: process.env.DB_PASSWORD || "",
            database: process.env.DB_NAME,
            entities: ['dist/**/*entity.js'],
            migrations: ['dist/db/migrations/*.js'],
            // synchronize: true,
            migrationsRun: true,
            // autoLoadEntities: true,

        }
    }
}
