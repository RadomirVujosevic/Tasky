import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AutherController } from './auther.controller';
import { AutherService } from './auther.service';
import { dbConfig } from './config/db.config';
import { jwtConfig } from './config/jwt.config';
import { Project } from './entities/project.entity';
import { Role } from './entities/role.entity';
import { Task } from './entities/task.entity';
import { User } from './entities/users.entity';
import { GoogleStrategy } from './google.strategy';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRootAsync(dbConfig),
    TypeOrmModule.forFeature([Role, Task, User, Project]),
    JwtModule.registerAsync(jwtConfig)
  ],
  controllers: [AutherController],
  providers: [AutherService, GoogleStrategy],
})
export class AutherModule { }
