import { Controller, Get } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { LoggerService } from './logger.service';

@Controller()
export class LoggerController {
  constructor(private readonly loggerService: LoggerService) { }

  @EventPattern('user_logged_in')
  loggedIn(email: string) {
    console.log(email)
  }
}
