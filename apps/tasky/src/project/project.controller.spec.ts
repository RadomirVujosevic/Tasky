import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from '../users/users.service';
import { JwtAuthGuard } from '../auth/jwt/jwt-auth.guard';
import { RolesGuard } from '../role/roles.guards';
import { ProjectController } from './project.controller';
import { ProjectService } from './project.service';
import { RoleEnum } from '../role/role.enum';

describe('ProjectController', () => {
  let controller: ProjectController;

  const getMockRequest = (role: RoleEnum) => {
    return {
      user: {
        role: role,
        id: 5
      }
    }
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProjectController],
      providers: [{
        provide: ProjectService,
        useValue: {
          create: jest.fn().mockImplementation((dto) => "Project created"),
          findAll: jest.fn().mockReturnValue("All projects"),
          findOneByID: jest.fn().mockImplementation((id) => "Mock Project"),
          update: jest.fn().mockImplementation((id, dto, req) => "Project Updated"),
          remove: jest.fn().mockReturnValue("Project Removed"),
          removeAll: jest.fn().mockReturnValue("All Projects Removed"),
        }
      },
      {
        provide: UsersService,
        useValue: {

        }
      }
      ],

    }).overrideGuard(JwtAuthGuard).useValue({})
      .overrideGuard(RolesGuard).useValue({})
      .compile();

    controller = module.get<ProjectController>(ProjectController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return all projects', () => {
    expect(controller.findAll()).toEqual("All projects")
  });

  it('should return project by id', () => {
    expect(controller.findOneByID(5)).toEqual("Mock Project")
  });

  it('should create a new task and return it', () => {
    expect(controller.create({ name: "Mock Task" }))
      .toEqual("Project created")
  });

  it('should update an existing task', () => {
    expect(controller.update(5, {}, getMockRequest(RoleEnum.Admin)))
      .resolves.toEqual("Project Updated")
  });

  it('should remove task by id', () => {
    expect(controller.remove(5)).toEqual("Project Removed")
  });

  it('should remove all tasks', () => {
    expect(controller.removeAll()).toEqual("All Projects Removed")
  });
});
