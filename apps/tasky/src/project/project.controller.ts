import { Controller, Get, Post, Body, Patch, Param, Delete, Req, UseGuards, ForbiddenException, Query, ValidationPipe, UsePipes, ParseBoolPipe, ParseArrayPipe, ParseIntPipe, HttpStatus, UseInterceptors } from '@nestjs/common';
import { ProjectService } from './project.service';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { JwtAuthGuard } from '../auth/jwt/jwt-auth.guard';
import { RolesGuard } from '../role/roles.guards';
import { Roles } from '../role/roles.decorator';
import { RoleEnum } from '../role/role.enum';
import { UsersService } from '../users/users.service';
import { Role } from '../role/entities/role.entity';
import { ApiParam, ApiQuery } from '@nestjs/swagger';
import { GetProjectTasksDTO } from './dto/get-project-tasks.dto';
import { query } from 'express';
import { TaskQueryDTO } from './dto/task-query.dto';
import { PaginationDTO } from '../api/pagination/pagination.dto';
import { PaginationInterceptor } from '../api/pagination/pagination.interceptor';

@Controller('projects')
@UseGuards(JwtAuthGuard, RolesGuard)
export class ProjectController {
  constructor(
    private readonly projectService: ProjectService,
    private usersService: UsersService
  ) { }

  @Roles(RoleEnum.Admin)
  @Post()
  create(@Body() createProjectDto: CreateProjectDto) {
    return this.projectService.create(createProjectDto);
  }

  @Roles(RoleEnum.Admin)
  @Get()
  findAll() {
    return this.projectService.findAll();
  }

  @Roles(RoleEnum.Admin)
  @Get(':id')
  findOneByID(@Param('id') id: number) {
    return this.projectService.findOneByID(id);
  }

  @UseInterceptors(PaginationInterceptor)
  @Roles(RoleEnum.Admin)
  @Get(':id/tasks?')
  async getProjectTasks(
    @Param('id', new ParseIntPipe({ errorHttpStatusCode: HttpStatus.BAD_REQUEST })) id: number,
    @Query() taskQuery: TaskQueryDTO,
    @Query() paginationQuery: PaginationDTO,
    @Query('completed', new ParseBoolPipe({})) completed: boolean,
    @Query('assignee', new ParseArrayPipe({ optional: true, items: Number })) assignees: number[],
    @Query('sortPriority') sortPriority: string
  ) {
    let tasks = await this.projectService.findTasks(id);

    if (taskQuery.title)
      return tasks.filter(task => task.title == taskQuery.title).at(0)

    if (completed)
      tasks = tasks.filter(task => task.completed == completed)

    if (assignees) {
      tasks = tasks.filter(task => assignees.includes(task.assignee.id))
    }

    tasks = tasks.sort((a, b) => a.priority == sortPriority ? -1 : 1)

    return tasks;
  }

  @Roles(RoleEnum.Manager)
  @Patch(':id')
  async update(
    @Param('id') id: number,
    @Body() updateProjectDto: UpdateProjectDto,
    @Req() req) {

    if (req.user.role == RoleEnum.Admin) {
      return this.projectService.update(id, updateProjectDto);
    }

    //Managers can only update projects that they are working on
    const myProjectIDs = (await this.usersService.findOneByID(req.user.id)).worksOn.map(proj => proj.id);
    if (!myProjectIDs.includes(id)) throw new ForbiddenException("You are not assigned to this project")

    return this.projectService.update(id, updateProjectDto);
  }

  @Roles(RoleEnum.Admin)
  @Delete(':id')
  remove(@Param() id: number) {
    return this.projectService.remove(+id);
  }

  @Roles(RoleEnum.Admin)
  @Delete()
  removeAll() {
    return this.projectService.removeAll();
  }
}
