

import { InjectRepository } from "@nestjs/typeorm";
import { Role } from "../role/entities/role.entity";
import { Repository } from "typeorm";
import { User } from "../users/entity/users.entity";
import { RoleEnum } from "../role/role.enum";
import { Injectable } from "@nestjs/common";
import { Project } from "./entities/project.entity";


export const getMockProjects = (manager: User): Project[] => {
    const project1: Project = {
        manager: manager,
        name: "Main Project",
        assignedUsers: [],
        id: 1,
    }

    const project2 = {
        manager: manager,
        name: "Side Project",
        assignedUsers: [],
        id: 2
    }

    return [project1, project2];
}
