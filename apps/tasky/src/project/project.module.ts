import { Global, Module } from '@nestjs/common';
import { ProjectService } from './project.service';
import { ProjectController } from './project.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Project } from './entities/project.entity';
import { Task } from '../task/entities/task.entity';
import { TaskModule } from '../task/task.module';
import { UsersModule } from '../users/users.module';

@Global()
@Module({
  controllers: [ProjectController],
  providers: [ProjectService],
  imports: [
    TypeOrmModule.forFeature([Project, Task]),
  ],
  exports: [ProjectService]
})
export class ProjectModule { }
