import { ForbiddenException, forwardRef, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from '../task/entities/task.entity';
import { TaskService } from '../task/task.service';
import { User } from '../users/entity/users.entity';
import { UsersService } from '../users/users.service';
import { Not, Repository } from 'typeorm';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { Project } from './entities/project.entity';

@Injectable()
export class ProjectService {
  constructor(
    @InjectRepository(Project) private projectRepo: Repository<Project>,

    @Inject(forwardRef(() => TaskService))
    private usersService: UsersService,

    @Inject(forwardRef(() => TaskService))
    private taskService: TaskService,
  ) { }
  defaultRelations = ['manager'];

  async create(createProjectDto: CreateProjectDto) {
    let manager: User = null;
    if (createProjectDto.manager != undefined)
      manager = await this.usersService.findOneByID(createProjectDto.manager);

    return this.projectRepo.save({
      assignedUsers: [],
      manager: manager,
      name: createProjectDto.name
    })

  }

  findAll() {
    return this.projectRepo.find({
      relations: this.defaultRelations
    });
  }

  async findTasksOnAssignedProjects(userID: number) {
    let tasks: Task[] = [];
    //get all projects assigned to this user
    const projects = await this.usersService.findUserProjects(userID);

    //find all tasks that are part of the projects
    for (let i = 0; i < projects.length; i++) {
      let tasksPerProject = await this.taskService.findByProject(projects[i])
      tasks.push(...tasksPerProject);
    }

    return tasks;
  }

  async findOneTaskOnAssignedProjects(userID: number, taskID: number) {
    const projects = await this.usersService.findUserProjects(userID);
    if (!projects) throw new NotFoundException("You have no projects");

    let task = await this.taskService.findOneByID(taskID);
    if (!task) throw new NotFoundException("Task does not exist");

    let projectIDs = projects.map(proj => proj.id);
    if (projectIDs.includes(task.project.id)) return task;

    throw new ForbiddenException();


  }

  async findTasks(projectID: number) {
    const project = await this.findOneByID(projectID);

    return this.taskService.findByProject(project)
  }

  async findTasksByCompletion(projectID: number, complete: boolean) {
    const tasks = await this.findTasks(projectID);
    return tasks.filter(task => task.completed == complete);
  }

  async findTasksByAssignees(projectID: number, userIDs: number[]) {
    const tasks = await this.findTasks(projectID);
    return tasks.filter(task => userIDs.includes(task.assignee.id))

  }

  async findOneByID(id: number) {
    const project = await this.projectRepo.find({
      where: { id },
      relations: this.defaultRelations
    })
    const proj = project[0];
    if (proj == undefined) throw new NotFoundException();

    return proj;
  }

  async findOneByName(name: string) {
    const project = await this.projectRepo.find({
      where: { name },
      relations: this.defaultRelations
    })
    return project;
  }

  async update(id: number, updateProjectDto: UpdateProjectDto) {
    let project = await this.findOneByID(id);
    const manager = await (updateProjectDto.manager ? this.usersService.findOneByID(updateProjectDto.manager) : project.manager);
    project.manager = manager
    project.name = updateProjectDto.name

    return this.projectRepo.update(id, project)
  }

  remove(id: number) {
    return `This action removes a #${id} project`;
  }

  removeAll() {
    return this.projectRepo.delete({});
  }
}
