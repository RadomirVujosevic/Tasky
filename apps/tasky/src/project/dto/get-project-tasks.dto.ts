import { ApiProperty } from '@nestjs/swagger'
import { IsBoolean } from 'class-validator';

export class GetProjectTasksDTO {
    @ApiProperty({
        type: [Number],
    })
    assignees?: number[];

    @ApiProperty({
        type: Boolean,
    })
    @IsBoolean()
    completed?: boolean;

    @ApiProperty({
        type: String,

    })
    name?: string
}