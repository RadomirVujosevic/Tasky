import { IsArray, IsBooleanString, IsIn, IsOptional, IsString } from "class-validator";

export class TaskQueryDTO {

    @IsString()
    @IsOptional()
    title: string;

    @IsBooleanString()
    @IsOptional()
    completed: boolean;

    @IsArray()
    @IsOptional()
    assignees: number[];

    @IsOptional()
    @IsIn(["high", "medium", "low"])
    sortPriority: string;
}