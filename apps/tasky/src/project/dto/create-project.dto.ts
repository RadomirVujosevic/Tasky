import { IsNotEmpty, IsNumber, IsOptional } from "class-validator";
import { User } from "../../users/entity/users.entity";

export class CreateProjectDto {

    @IsOptional()
    manager?: number;

    @IsNotEmpty()
    name: string;
}
