export default () => ({
    jwtSecret: process.env.JWT_SECRET,
    dbUsername: process.env.USERNAME,
})