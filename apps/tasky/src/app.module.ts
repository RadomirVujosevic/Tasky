import { Global, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { TaskModule } from './task/task.module';
import { ProjectModule } from './project/project.module';
import { RoleModule } from './role/role.module';
import { RolesGuard } from './role/roles.guards';
import { dbConfig } from './config/db.config';
import { Role } from './role/entities/role.entity';
import { Task } from './task/entities/task.entity';
import { User } from './users/entity/users.entity';
import { Project } from './project/entities/project.entity';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Global()
@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRootAsync(dbConfig),
    UsersModule,
    AuthModule,
    TaskModule,
    ProjectModule,
    RoleModule,
    TypeOrmModule.forFeature([Role, Task, User, Project]),
    ClientsModule.register([
      {
        name: 'LOGGER',
        transport: Transport.REDIS,
        options: {
          host: 'localhost',
          port: 6379
        }
      },
    ])
  ],
  controllers: [AppController],
  providers: [AppService,
    RolesGuard],
})
export class AppModule { }
