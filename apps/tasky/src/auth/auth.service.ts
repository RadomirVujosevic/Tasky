import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import { LoginUserDto } from './dto/login-user.dto';

@Injectable()
export class AuthService {
    constructor(private usersService: UsersService, private jwtService: JwtService) { }

    async validateUser(email: string, password: string): Promise<any> {
        const user = await this.usersService.findOneByEmail(email);
        if (user && user.password == password) {
            const { password, ...rest } = user;
            return rest;
        }
        return null;
    }

    googleLogin(req) {
        if (!req.user) {
            throw new BadRequestException();
        }
        return req.user
    }

    async login(user: any) {

        const payload = {
            email: user.email,
            id: user.id,
            role: user.role.name
        };

        return {
            access_token: this.jwtService.sign(payload)
        }
    }
}
