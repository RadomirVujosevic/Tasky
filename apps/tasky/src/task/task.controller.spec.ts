import { Test, TestingModule } from '@nestjs/testing';
import { JwtAuthGuard } from '../auth/jwt/jwt-auth.guard';
import { RoleEnum } from '../role/role.enum';
import { RolesGuard } from '../role/roles.guards';
import { UsersService } from '../users/users.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { TaskController } from './task.controller';
import { TaskService } from './task.service';

describe('TaskController', () => {
  let controller: TaskController;

  const getMockRequest = (role: RoleEnum) => {
    return {
      user: {
        role: role,
        id: 5
      }
    }
  }
  const getMockTask = (): CreateTaskDto => {
    return {
      title: "",
      completed: false,
      description: "",
      priority: "high"

    }
  }
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TaskController],
      providers: [JwtAuthGuard, RolesGuard, {
        provide: TaskService,
        useValue: {
          create: jest.fn().mockImplementation((dto, req) => "Task created"),
          findAll: jest.fn().mockResolvedValue("All tasks"),
          findOneByID: jest.fn().mockResolvedValue("Mock Task"),
          update: jest.fn().mockResolvedValue("Task Updated"),
          remove: jest.fn().mockResolvedValue("Task Removed"),
          removeAll: jest.fn().mockResolvedValue("All Tasks Removed"),
        },
      },
        {
          provide: UsersService,
          useValue: {
          }
        }
      ],
    }).overrideGuard(JwtAuthGuard).useValue({})
      .overrideGuard(RolesGuard).useValue({})
      .compile();

    controller = module.get<TaskController>(TaskController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('Should create user', () => {
    expect(controller.create(getMockTask(), getMockRequest(RoleEnum.Admin)))
      .toEqual("Task created");
  });

  it('Should return an array of all users', () => {
    expect(controller.findAll(getMockRequest(RoleEnum.Admin), { limit: 1, page: 1 }))
      .resolves.toEqual("All users");
  });

  it('should return a tasky by id', () => {
    expect(controller.findOne(5, getMockRequest(RoleEnum.Admin)))
      .resolves.toEqual("Mock Task")
  });

  it('should update task by id', () => {
    expect(controller.update(5, {}, getMockRequest(RoleEnum.Admin)))
      .resolves.toEqual("Task Updated");
  });

  it('should update task by id', () => {
    expect(controller.remove(5, getMockRequest(RoleEnum.Admin)))
      .resolves.toEqual("Task Removed");
  });

  it('should update task by id', () => {
    expect(controller.removeAll())
      .resolves.toEqual("All Tasks Removed");
  });

});
