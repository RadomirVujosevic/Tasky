import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { getMockData } from '../app.mock.data';
import { dbConfig } from '../config/db.config';
import { Project } from '../project/entities/project.entity';
import { ProjectService } from '../project/project.service';
import { Role } from '../role/entities/role.entity';
import { RoleService } from '../role/role.service';
import { User } from '../users/entity/users.entity';
import { UsersService } from '../users/users.service';
import { Task } from './entities/task.entity';
import { TaskService } from './task.service';

describe('TaskService', () => {
  let service: TaskService;
  let mockData = getMockData();
  const randomUser = mockData.users[Math.floor(Math.random() * (mockData.users.length - 1))]
  const randomProject = mockData.projects[Math.floor(Math.random() * (mockData.projects.length - 1))]

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forRootAsync({ ...dbConfig, })],
      providers: [TaskService,
        {
          provide: getRepositoryToken(Task),
          useValue: {
            find: jest.fn().mockImplementation(
              (options?: {
                where?: {
                  id?: number,
                  title?: string,
                  assignee?: User,
                  project?: Project
                }
              }): Task[] => {
                let tasks = mockData.tasks;
                if (options.where && options.where.id) {
                  tasks = tasks.filter(task => task.id == options.where.id);
                }

                if (options.where && options.where.title) {
                  tasks = tasks.filter(task => task.title == options.where.title);
                }

                if (options.where && options.where.assignee) {
                  tasks = tasks.filter(task => task.assignee.id == options.where.assignee.id);
                }

                if (options.where && options.where.project) {
                  tasks = tasks.filter(task => task.project.id == options.where.project.id);
                }

                return tasks;
              })
          }
        },
        {
          provide: UsersService,
          useValue: {
            findOneByID: jest.fn().mockImplementation(id => {
              return mockData.users.filter(user => user.id == id).at(0);
            })
          }
        },
        {
          provide: ProjectService,
          useValue: {

          }
        },
        {
          provide: RoleService,
          useValue: {

          }
        }

      ]
      ,
    }).compile();

    service = module.get<TaskService>(TaskService);

  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return all tasks', async () => {
    expect(service.findAll()).toEqual(mockData.tasks);
  });

  it('should return one task by id', async () => {
    expect(service.findOneByID(mockData.tasks[0].id))
      .resolves.toEqual(mockData.tasks[0]);

    expect(service.findOneByID(-1))
      .resolves.toBeUndefined()
  });

  it('should return one task by title', async () => {
    expect(service.findOneByName(mockData.tasks[0].title))
      .resolves.toEqual(mockData.tasks[0]);

    expect(service.findOneByName("Random string"))
      .resolves.toBeUndefined();
  });

  it('should return tasks assigned to user ', async () => {
    expect(service.findByAssignee(randomUser.id))
      .resolves.toEqual(mockData.tasks.filter(task => task.assignee.id == randomUser.id));

    expect(service.findByAssignee(-1))
      .resolves.toEqual([]);
  });

  it('should return tasks assigned to a project', async () => {
    expect(service.findByProject(randomProject))
      .resolves.toEqual(mockData.tasks.filter(task => task.project.id == randomProject.id));

    expect(service.findByAssignee(-1))
      .resolves.toEqual([]);
  });

});
