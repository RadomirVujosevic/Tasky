import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Project } from "../project/entities/project.entity";
import { Role } from "../role/entities/role.entity";
import { User } from "../users/entity/users.entity";
import { Repository } from "typeorm";
import { Task } from "./entities/task.entity";


export const getMockTasks = (admin: User, grunt: User, radomir: User, mainProject: Project, sideProject: Project): Task[] => {
    let task1: Task = {
        title: "Task1",
        completed: false,
        description: "",
        priority: "high",
        creator: admin,
        assignee: radomir,
        project: mainProject,
        id: 1

    }

    let task2: Task = {
        title: "Task2",
        completed: true,
        description: "",
        priority: "medium",
        creator: admin,
        assignee: radomir,
        project: mainProject,
        id: 2
    }

    let task3: Task = {
        title: "Task3",
        completed: true,
        description: "",
        priority: "high",
        creator: admin,
        assignee: grunt,
        project: sideProject,
        id: 3
    }

    let task4: Task = {
        title: "Task4",
        completed: true,
        description: "",
        priority: "low",
        creator: admin,
        assignee: grunt,
        project: mainProject,
        id: 4
    }

    return [task1, task2, task3, task4];
}
