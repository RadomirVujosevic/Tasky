import { IsBoolean, IsIn, IsNotEmpty, IsObject, IsOptional, IsString, ValidateIf } from "class-validator";

export class CreateTaskDto {
    @IsNotEmpty()
    title: string;

    @IsOptional()
    description: string = "";

    @IsOptional()
    @IsBoolean()
    completed: boolean = false;

    @IsString()
    @IsOptional()
    @IsIn(["high", "medium", "low"])
    priority: "high" | "medium" | "low" = "medium";

}
