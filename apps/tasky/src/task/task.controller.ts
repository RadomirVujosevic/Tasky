import { Controller, Get, Post, Body, Patch, Param, Delete, Req, UseGuards, ForbiddenException, Query, ValidationPipe, UseInterceptors } from '@nestjs/common';
import { TaskService } from './task.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { JwtAuthGuard } from '../auth/jwt/jwt-auth.guard';
import { UsersService } from '../users/users.service';
import { Roles } from '../role/roles.decorator';
import { RoleEnum } from '../role/role.enum';
import { ProjectService } from '../project/project.service';
import { RolesGuard } from '../role/roles.guards';
import { PaginationDTO } from '../api/pagination/pagination.dto';
import { PaginationInterceptor } from '../api/pagination/pagination.interceptor';

@Controller('tasks')
@UseGuards(JwtAuthGuard, RolesGuard)
export class TaskController {
  constructor(
    private readonly taskService: TaskService,
    private readonly userService: UsersService,
  ) { }

  @Post()
  create(@Body() createTaskDto: CreateTaskDto, @Req() req) {
    //current user is always set as creator
    return this.taskService.create(createTaskDto, req.user.id);
  }

  @UseInterceptors(PaginationInterceptor)
  @Get()
  async findAll(
    @Req() req,
    @Query(new ValidationPipe()) pagingQuery: PaginationDTO) {
    const allTasks = await this.taskService.findAll();
    if (req.user.role == RoleEnum.Admin) {
      return allTasks;
    }

    const user = await this.userService.findOneByID(req.user.id);
    const projectIDs = user.worksOn.map(proj => proj.id);

    return allTasks.filter(task => task.assignee.id == user.id || projectIDs.includes(task.project.id))
  }

  @Get(':id')
  async findOne(@Param('id') taskID: number, @Req() req) {
    const task = await this.taskService.findOneByID(taskID);

    if (!this.userCanModifyTask(taskID, req.user))
      throw new ForbiddenException();

    return task;
  }

  @Patch(':id')
  async update(@Param('id') taskID: number, @Body() updateTaskDto: UpdateTaskDto, @Req() req) {
    if (!this.userCanModifyTask(taskID, req.user))
      throw new ForbiddenException();

    return this.taskService.update(+taskID, updateTaskDto);

  }

  @Delete(':id')
  remove(@Param('id') taskID: number, @Req() req) {
    if (!this.userCanModifyTask(taskID, req.user))
      throw new ForbiddenException();

    return this.taskService.remove(taskID);
  }

  @Roles(RoleEnum.Admin)
  @UseGuards(JwtAuthGuard)
  @Delete()
  removeAll() {
    return this.taskService.removeAll();
  }

  //Employee and manager users can view, patch, delete and create tasks but only if those tasks are part of the same project as the user
  //Admins can always do this
  async userCanModifyTask(taskID: number, user: any) {

    //Admins can view all tasks
    if (user.role == RoleEnum.Admin)
      return true;

    const task = await this.taskService.findOneByID(taskID);

    //Users can modify tasks that are assigned to them
    if (task.assignee.id == user.id)
      return true;

    //Users can also modify tasks assigned to other users if they are part of the same project
    const myProjects = (await this.userService.findOneByID(user.id)).worksOn;
    const myProjectIDs = myProjects.map(proj => proj.id);
    if (myProjectIDs.includes(task.project.id))
      return true;

    throw false
  }
}
