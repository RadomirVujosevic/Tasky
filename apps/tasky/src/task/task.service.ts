import { ConflictException, forwardRef, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Project } from '../project/entities/project.entity';
import { ProjectService } from '../project/project.service';
import { UsersService } from '../users/users.service';
import { Repository } from 'typeorm';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { Task } from './entities/task.entity';

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(Task) private taskRepo: Repository<Task>,

    @Inject(forwardRef(() => UsersService))
    private usersService: UsersService,

    @Inject(forwardRef(() => ProjectService))
    private projectService: ProjectService
  ) { }

  defaultRelations = ['assignee', 'creator', 'project'];

  async createDefault(createTaskDto: CreateTaskDto) {
    let newTask = this.taskRepo.create(createTaskDto);
    const creator = await this.usersService.findOneByID(1);
    newTask.creator = creator;
    return this.taskRepo.save(newTask)
  }

  async create(createTaskDto: CreateTaskDto, creatorID: number) {
    //If task already exists, inform the user and reject request
    //Can be omitted and it will result in a 500 error 
    let oldTask = await this.findOneByName(createTaskDto.title);
    if (oldTask) throw new ConflictException("Title already exists");

    let newTask = this.taskRepo.create(createTaskDto);
    const creator = await this.usersService.findOneByID(creatorID);
    newTask.creator = creator

    return this.taskRepo.save(newTask)
  }

  findAll() {

    return this.taskRepo.find({
      relations: this.defaultRelations
    });
  }

  async findOneByID(id: number): Promise<Task> {
    const task = await this.taskRepo.find({
      where: { id },
      relations: this.defaultRelations
    });
    if (!task) return undefined;

    return task[0];
  }

  async findOneByName(title: string) {
    const task = (await this.taskRepo.find({
      take: 1,
      where: { title },
      relations: this.defaultRelations
    }));

    if (!task) return undefined;

    return task[0];
  }

  async findByAssignee(userID: number): Promise<Task[]> {
    const user = await this.usersService.findOneByID(userID);
    if (!user) return [];

    return await this.taskRepo.find({
      relations: this.defaultRelations,
      where: {
        assignee: user
      }
    })
  }

  async findByProject(project: Project) {
    return this.taskRepo.find({
      where: { project },
      relations: this.defaultRelations

    },
    )
  }

  async update(id: number, updateTaskDto: UpdateTaskDto) {

    const task = await this.findOneByID(id);
    if (!task) throw new NotFoundException();
    const { creator, assignee, project, ...rest } = updateTaskDto
    task.creator = await (creator ? this.usersService.findOneByID(creator) : task.creator);
    task.assignee = await (assignee ? this.usersService.findOneByID(assignee) : task.assignee);
    task.project = await (project ? this.projectService.findOneByID(project) : task.project);

    return this.taskRepo.save({ ...task, ...rest })
  }

  async remove(id: number) {
    const task = await this.findOneByID(id);
    return this.taskRepo.remove(task);
  }

  async removeAll() {
    return this.taskRepo.delete({});
  }
}
