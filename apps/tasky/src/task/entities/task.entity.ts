import { Project } from "../../project/entities/project.entity";
import { User } from "../../users/entity/users.entity";
import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Task {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => User, (user) => user.id, { onDelete: 'SET NULL' })
    assignee: User;

    //Not sure what onDelete policy makes sense
    //If a creator is deleted, it makes sense that his tasks still exist, so on delete SET NULL makes sense
    //On the other hand, the specification requires creator to be NOT NULL
    @ManyToOne(() => User, (user) => user.id, { onDelete: 'SET NULL', nullable: false })
    creator: User;

    @ManyToOne(() => Project, (project) => project.id, { onDelete: 'SET NULL' })
    project: Project

    @Column({
        length: 10,
        nullable: false
    })
    priority: string;

    @Column({
        length: 100,
        nullable: false,
        unique: true,
    })
    title: string;

    @Column({
        length: 250,
        nullable: true,
        unique: false,
    })
    description: string | null;

    @Column({
        nullable: false
    })
    completed: boolean

}
