import { Controller, Get, Post, Req, UseGuards, Request, Delete, Param, Body, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { AuthGuard } from '@nestjs/passport';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
import { LoginUserDto } from './auth/dto/login-user.dto';
import { JwtAuthGuard } from './auth/jwt/jwt-auth.guard';
import { LocalAuthGuard } from './auth/local-auth.guard';
import { CreateUserDto } from './users/dto/create-user.dto';
import { UsersService } from './users/users.service';

@Controller()
export class AppController {
  constructor(
    private readonly authService: AuthService,
    private appService: AppService,
    private usersService: UsersService,
    @Inject('LOGGER') private client: ClientProxy) { }

  //gets redirected to googleAuthRedirect
  @UseGuards(AuthGuard('google'))
  @Get('login')
  googleLogin(@Request() req): any {
  }

  @Get('auth/google/callback')
  @UseGuards(AuthGuard('google'))
  googleAuthRedirect(@Req() req) {
    const res = this.client.send('googleLogin', req.user)
    res.subscribe({
      next(res) {
        console.log("res", res);
      },
      error(err) {
        console.log("err", err);
      },
      complete() {
        console.log("done");

      }
    })

    return this.authService.googleLogin(req)
  }

  @Post('register')
  create(@Body() createTaskDto: CreateUserDto) {
    return this.usersService.createDefault(createTaskDto);
  }

  @Delete('seed')
  async unseedDB(@Request() req): Promise<any> {
    await this.appService.unseed();
    return "unseed successful";
  }


  @Get('seed')
  async seedDB(@Request() req): Promise<any> {
    await this.appService.seed();
    return "seed successful";
  }

}
