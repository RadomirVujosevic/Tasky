import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { ProjectService } from './project/project.service';
import { RoleService } from './role/role.service';
import { TaskService } from './task/task.service';
import { UsersService } from './users/users.service';
import { RoleEnum } from './role/role.enum';
import { Role } from './role/entities/role.entity';
import { Project } from './project/entities/project.entity';
import { getMockData } from './app.mock.data';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Task } from './task/entities/task.entity';
import { User } from './users/entity/users.entity';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(Role) private roleRepo: Repository<Role>,
    @InjectRepository(Task) private taskRepo: Repository<Task>,
    @InjectRepository(Project) private projectRepo: Repository<Project>,
    @InjectRepository(User) private usersRepo: Repository<User>,

  ) {
  }

  async unseed() {
    await this.taskRepo.delete({});
    await this.usersRepo.delete({});
    await this.projectRepo.delete({});
    await this.roleRepo.delete({});
  }

  async seed() {

    const data = getMockData();

    await this.roleRepo.save(data.roles);
    await this.usersRepo.save(data.users);
    await this.projectRepo.save(data.projects);
    await this.taskRepo.save(data.tasks);

  }

}
