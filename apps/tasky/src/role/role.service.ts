import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Role } from './entities/role.entity';
import { RoleEnum } from './role.enum';

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(Role) private roleRepo: Repository<Role>
  ) { }

  create(createRoleDto: CreateRoleDto) {
    return this.roleRepo.save(createRoleDto);
  }

  findAll() {
    return this.roleRepo.find();
  }

  async findOne(name: RoleEnum) {
    return (await this.roleRepo.find({ where: { name } }))[0];
  }

  update(id: number, updateRoleDto: UpdateRoleDto) {
    return `This action updates a #${id} role`;
  }

  remove(id: number) {
    return this.roleRepo.delete({ id });
  }

  removeAll() {
    return this.roleRepo.delete({});
  }
}
