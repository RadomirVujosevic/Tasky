import { RoleEnum } from "../role.enum";

export class CreateRoleDto {
    name: RoleEnum;
}
