import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "../users/entity/users.entity";
import { Repository } from "typeorm";
import { Role } from "./entities/role.entity";
import { RoleEnum } from "./role.enum";


export const getMockRoles = (): Role[] => {
    let employeeRole: Role = {
        name: RoleEnum.Employee,
        id: 1,
        users: []
    };
    let managerRole: Role = {
        name: RoleEnum.Manager,
        id: 2,
        users: []
    };
    let adminRole: Role = {
        name: RoleEnum.Admin,
        id: 3,
        users: []
    };

    return [employeeRole, managerRole, adminRole];

}
