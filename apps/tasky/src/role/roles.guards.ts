import { CanActivate, ExecutionContext, Injectable, UseGuards } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { Observable } from "rxjs";
import { RoleEnum } from "./role.enum";
import { ROLES_KEY } from "./roles.decorator";

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private reflector: Reflector) { }

    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {

        const requiredRoles = this.reflector.getAllAndOverride<RoleEnum[]>
            (ROLES_KEY, [
                context.getHandler(),
                context.getClass()
            ]
            )

        if (!requiredRoles) {
            return true;
        }
        const { user } = context.switchToHttp().getRequest();
        const userRole = user.role;

        //Admin can do anything
        if (userRole == "Admin") return true;
        //Manager can do anything that is marked as Manager or Employee
        else if (userRole == "Manager" && requiredRoles.some(role => role == RoleEnum.Manager || role == RoleEnum.Employee)) return true
        else if (requiredRoles.includes(RoleEnum.Employee)) return true;

        return false;

    }
}