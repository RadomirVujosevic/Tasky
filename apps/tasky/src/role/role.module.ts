import { Global, Module } from '@nestjs/common';
import { RoleService } from './role.service';
import { RoleController } from './role.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from './entities/role.entity';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from './roles.guards';

@Global()
@Module({
  controllers: [RoleController],
  providers: [RoleService],
  imports: [
    TypeOrmModule.forFeature([Role])
  ],
  exports: [
    RoleService
  ]
})
export class RoleModule { }
