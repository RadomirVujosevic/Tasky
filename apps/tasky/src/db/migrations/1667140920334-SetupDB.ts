import { MigrationInterface, QueryRunner } from "typeorm";

export class SetupDB1667140920334 implements MigrationInterface {
    name = 'SetupDB1667140920334'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."role_name_enum" AS ENUM('Employee', 'Admin', 'Manager')`);
        await queryRunner.query(`CREATE TABLE "role" ("id" SERIAL NOT NULL, "name" "public"."role_name_enum" NOT NULL, CONSTRAINT "PK_b36bcfe02fc8de3c57a8b2391c2" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "task" ("id" SERIAL NOT NULL, "priority" character varying(10) NOT NULL, "title" character varying(100) NOT NULL, "description" character varying(250), "completed" boolean NOT NULL, "assigneeId" integer, "creatorId" integer NOT NULL, "projectId" integer, CONSTRAINT "UQ_3399e2710196ea4bf734751558f" UNIQUE ("title"), CONSTRAINT "PK_fb213f79ee45060ba925ecd576e" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user" ("id" SERIAL NOT NULL, "email" character varying(250) NOT NULL, "first_name" character varying(250) NOT NULL, "last_name" character varying(250) NOT NULL, "gender" character varying(10) NOT NULL, "password" character varying(50) NOT NULL, "phone" character varying(10), "roleId" integer, CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "project" ("id" SERIAL NOT NULL, "name" text NOT NULL, "managerId" integer, CONSTRAINT "UQ_dedfea394088ed136ddadeee89c" UNIQUE ("name"), CONSTRAINT "PK_4d68b1358bb5b766d3e78f32f57" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user_works_on_project" ("userId" integer NOT NULL, "projectId" integer NOT NULL, CONSTRAINT "PK_46c89a046093f8ae2e684e6e997" PRIMARY KEY ("userId", "projectId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_442b79162444713d7d2b986a79" ON "user_works_on_project" ("userId") `);
        await queryRunner.query(`CREATE INDEX "IDX_b7a6959cc6daac1449b7e97c52" ON "user_works_on_project" ("projectId") `);
        await queryRunner.query(`ALTER TABLE "task" ADD CONSTRAINT "FK_7384988f7eeb777e44802a0baca" FOREIGN KEY ("assigneeId") REFERENCES "user"("id") ON DELETE SET NULL ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "task" ADD CONSTRAINT "FK_94fe6b3a5aec5f85427df4f8cd7" FOREIGN KEY ("creatorId") REFERENCES "user"("id") ON DELETE SET NULL ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "task" ADD CONSTRAINT "FK_3797a20ef5553ae87af126bc2fe" FOREIGN KEY ("projectId") REFERENCES "project"("id") ON DELETE SET NULL ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "FK_c28e52f758e7bbc53828db92194" FOREIGN KEY ("roleId") REFERENCES "role"("id") ON DELETE SET NULL ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "project" ADD CONSTRAINT "FK_33a588338bd946c9295c316d4bb" FOREIGN KEY ("managerId") REFERENCES "user"("id") ON DELETE SET NULL ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_works_on_project" ADD CONSTRAINT "FK_442b79162444713d7d2b986a795" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "user_works_on_project" ADD CONSTRAINT "FK_b7a6959cc6daac1449b7e97c525" FOREIGN KEY ("projectId") REFERENCES "project"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_works_on_project" DROP CONSTRAINT "FK_b7a6959cc6daac1449b7e97c525"`);
        await queryRunner.query(`ALTER TABLE "user_works_on_project" DROP CONSTRAINT "FK_442b79162444713d7d2b986a795"`);
        await queryRunner.query(`ALTER TABLE "project" DROP CONSTRAINT "FK_33a588338bd946c9295c316d4bb"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "FK_c28e52f758e7bbc53828db92194"`);
        await queryRunner.query(`ALTER TABLE "task" DROP CONSTRAINT "FK_3797a20ef5553ae87af126bc2fe"`);
        await queryRunner.query(`ALTER TABLE "task" DROP CONSTRAINT "FK_94fe6b3a5aec5f85427df4f8cd7"`);
        await queryRunner.query(`ALTER TABLE "task" DROP CONSTRAINT "FK_7384988f7eeb777e44802a0baca"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_b7a6959cc6daac1449b7e97c52"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_442b79162444713d7d2b986a79"`);
        await queryRunner.query(`DROP TABLE "user_works_on_project"`);
        await queryRunner.query(`DROP TABLE "project"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "task"`);
        await queryRunner.query(`DROP TABLE "role"`);
        await queryRunner.query(`DROP TYPE "public"."role_name_enum"`);
    }

}
