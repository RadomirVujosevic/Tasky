import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtAuthGuard } from '../auth/jwt/jwt-auth.guard';
import { RoleModule } from '../role/role.module';
import { RolesGuard } from '../role/roles.guards';
import { TaskModule } from '../task/task.module';
import { User } from './entity/users.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

@Global()
@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    RoleModule
  ],
  providers: [UsersService, RolesGuard, JwtAuthGuard],
  exports: [UsersService],
  controllers: [UsersController]
})
export class UsersModule { }
