import { Controller, Get, Post, Body, Patch, Param, Delete, NotFoundException, UseGuards, Req, ForbiddenException, ParseIntPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../auth/jwt/jwt-auth.guard';
import { RoleEnum } from '../role/role.enum';
import { Roles } from '../role/roles.decorator';
import { RolesGuard } from '../role/roles.guards';
import { UpdateUserDto } from './dto/update-user.dto';
import { UsersService } from './users.service';

@Controller('users')
@UseGuards(JwtAuthGuard, RolesGuard)
export class UsersController {
    constructor(private readonly usersService: UsersService) { }


    @Roles(RoleEnum.Admin)
    @Get()
    async findAll() {
        const users = await this.usersService.findAll();
        return users;
    }

    //gets info about a specific user
    @Get(':id')
    async findOne(@Param('id', new ParseIntPipe()) id: number, @Req() req) {
        const user = await this.usersService.findOneByID(id);
        if (user == null) throw new NotFoundException()

        if (user.id == id)
            return user;

        //Admin can always read user info
        if (req.user.role === RoleEnum.Admin) {
            return user;
        }

        //users are only allowed to get info about other users if they are assigned to the same project
        const myProjects = (await this.usersService.findOneByID(req.user.id)).worksOn;
        const myProjectIDs = myProjects.map(proj => proj.id);

        const userProjectIDs = user.worksOn.map(proj => proj.id);

        //if there is no interserction then the request is forbidden
        const interserction = myProjectIDs.filter(myID => userProjectIDs.includes(myID));
        if (interserction.length == 0) throw new ForbiddenException();

        return user;
    }

    @Get(':id/tasks')
    async findUserTasks(@Param('id', new ParseIntPipe()) id: number, @Req() req) {
        return await this.usersService.findUserTasks(id);
    }

    @Get('projects')
    async findUserProjects(@Param('id') id: number, @Req() req) {
        return await this.usersService.findUserProjects(req.user.id);
    }

    @Roles(RoleEnum.Admin)
    @Patch(':id')
    update(@Param('id') id: number, @Body() updateTaskDto: UpdateUserDto) {
        return this.usersService.update(+id, updateTaskDto);
    }

    @Roles(RoleEnum.Admin)
    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.usersService.remove(id);
    }

    @Roles(RoleEnum.Admin)
    @Delete()
    removeAll() {
        return this.usersService.removeAll();
    }
}
