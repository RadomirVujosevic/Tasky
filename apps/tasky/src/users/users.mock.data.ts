import { InjectRepository } from "@nestjs/typeorm";
import { Role } from "../role/entities/role.entity";
import { Repository } from "typeorm";
import { User } from "./entity/users.entity";
import { Injectable } from "@nestjs/common";



export const getMockUsers = (employeeRole: Role, managerRole: Role, adminRole: Role): User[] => {

    let admin: User = {
        email: "admin@gmail.com",
        first_name: "Mr Admin",
        last_name: "Admins dont need a last name",
        gender: "Male",
        password: "1234",
        role: adminRole,
        createdTasks: [],
        id: 1,
        managedProjects: [],
        phone: "",
        tasks: [],
        worksOn: []
    }

    let grunt: User = {
        email: "grunt@gmail.com",
        first_name: "Grunt",
        last_name: "Grunty",
        gender: "Male",
        password: "12",
        role: employeeRole,
        createdTasks: [],
        id: 2,
        managedProjects: [],
        phone: "",
        tasks: [],
        worksOn: []
    }

    let radomir: User = {
        email: "radvuj@gmail.com",
        first_name: "Radomir",
        last_name: "Vujosevic",
        gender: "Male",
        password: "12",
        role: managerRole,
        createdTasks: [],
        id: 3,
        managedProjects: [],
        phone: "",
        tasks: [],
        worksOn: []
    }

    return [admin, grunt, radomir];
}
