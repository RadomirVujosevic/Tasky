import { TestingModule, Test } from "@nestjs/testing";
import { RoleEnum } from "../role/role.enum";
import { JwtAuthGuard } from "../auth/jwt/jwt-auth.guard";
import { RolesGuard } from "../role/roles.guards";
import { UsersController } from "./users.controller";
import { UsersService } from "./users.service";


//Controller tests just check to see if the correct service functions are called
//Service unit tests check for correctness

describe('UserController', () => {
    let controller: UsersController;

    const getMockRequest = (role: RoleEnum) => {
        return {
            user: {
                role: role,
                id: 5
            }
        }
    }


    beforeEach(async () => {


        const module: TestingModule = await Test.createTestingModule({
            controllers: [UsersController],
            providers: [JwtAuthGuard, RolesGuard,
                {
                    provide: UsersService,
                    useValue: {
                        findAll: jest.fn().mockResolvedValue("Mock User"),
                        findOneByID: jest.fn().mockImplementation(() => "Mock User"),
                        remove: jest.fn().mockResolvedValue("User removed"),
                        removeAll: jest.fn().mockResolvedValue("All users removed"),
                        update: jest.fn().mockImplementation((id, dto) => "User updated"),
                        findUserTasks: jest.fn().mockImplementation((id) => "Tasks assigned to user"),
                        findUserProjects: jest.fn().mockImplementation((id) => "Projects user works on")
                    }
                },
            ],
        }).overrideGuard(JwtAuthGuard).useValue({})
            .overrideGuard(RolesGuard).useValue({})
            .compile();

        controller = module.get<UsersController>(UsersController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });

    it('findAll should return a list of users from usersService', () => {
        expect(controller.findAll()).resolves.toEqual("Mock User")
    })

    it('findOne should return one user', () => {
        expect(controller.findOne(5, getMockRequest(RoleEnum.Admin)))
            .resolves.toEqual("Mock User")
    })

    it('Remove a user by id', () => {
        expect(controller.remove(5)).resolves.toEqual("User removed")
    })

    it('Remove all users', () => {
        expect(controller.removeAll()).resolves.toEqual("All users removed")
    })

    it('Update user by id', () => {
        expect(controller.update(5, {})).toEqual("User updated")
    })

    it('Update user by id', () => {
        expect(controller.findUserTasks(5, getMockRequest(RoleEnum.Admin)))
            .resolves.toEqual("Tasks assigned to user")
    })

    it('Update user by id', () => {
        expect(controller.findUserProjects(5, getMockRequest(RoleEnum.Admin)))
            .resolves.toEqual("Projects user works on")
    })


});