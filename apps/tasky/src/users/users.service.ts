import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProjectService } from '../project/project.service';
import { Role } from '../role/entities/role.entity';
import { RoleEnum } from '../role/role.enum';
import { RoleService } from '../role/role.service';
import { TaskService } from '../task/task.service';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entity/users.entity';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User) private usersRepository: Repository<User>,
        private roleService: RoleService,

        @Inject(forwardRef(() => TaskService))
        private taskService: TaskService,

        @Inject(forwardRef(() => ProjectService))
        private projectService: ProjectService
    ) { }

    defaultRelations = ['role', 'worksOn'];

    create(createUserDto: CreateUserDto, role: Role) {
        const newUser = this.usersRepository.create(createUserDto);
        newUser.role = role;
        newUser.worksOn = []
        return this.usersRepository.save(newUser);
    }

    async createDefault(createUserDto: CreateUserDto) {
        let newUser = this.usersRepository.create(createUserDto);
        const employeeRole = await this.roleService.findOne(RoleEnum.Employee);
        newUser.role = employeeRole
        return this.usersRepository.save(newUser);
    }

    async findUserTasks(userID: number) {
        return await this.taskService.findByAssignee(userID);
    }

    async findUserProjects(userID: number) {
        const user = await this.findOneByID(userID);

        if (!user) return [];

        return user.worksOn;
    }

    findAll() {
        return this.usersRepository.find({
            relations: this.defaultRelations,
        });
    }

    async findOneByID(id: number) {
        const user = await this.usersRepository.find({
            where: { id },
            relations: this.defaultRelations
        });
        if (!user) return null;
        return user[0];

    }

    async findOneByEmail(email: string) {
        return (await this.usersRepository.find({
            where: { email },
            relations: ['role']
        }))[0];
    }


    async update(id: number, updateUserDto: UpdateUserDto) {
        let user = await this.findOneByID(id);
        const { tasks, role_id, projects, ...rest } = updateUserDto;

        if (tasks) {
            user.tasks = []
            for (let i = 0; i < tasks.length; i++) {
                const task = await this.taskService.findOneByID(tasks[i]);
                if (task)
                    user.tasks.push(task);
            }
        }

        if (projects) {
            user.worksOn = []
            for (let i = 0; i < projects.length; i++) {
                const project = await this.projectService.findOneByID(projects[i]);
                if (project)
                    user.worksOn.push(project);
            }
        }

        if (role_id) {
            user.role = await this.roleService.findOne(role_id);
        }


        return this.usersRepository.save({ ...user, ...rest });
    }

    async remove(id: number) {
        const user = await this.findOneByID(id);
        return this.usersRepository.remove(user);
    }

    async removeAll() {
        return this.usersRepository.delete({});
    }

}
