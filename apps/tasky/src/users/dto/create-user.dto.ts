import { IsEmail, IsIn, IsNotEmpty, IsOptional } from "class-validator";

export class CreateUserDto {
    @IsEmail()
    email: string;

    @IsNotEmpty()
    first_name: string;

    @IsNotEmpty()
    last_name: string;

    @IsIn(["Male", "Female"])
    gender: string;

    @IsNotEmpty()
    password: string;

    @IsOptional()
    phone?: string;

}