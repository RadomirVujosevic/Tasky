import { CreateUserDto } from "./create-user.dto";
import { OmitType, PartialType } from '@nestjs/mapped-types';
import { IsArray, IsIn, IsNumber, IsOptional } from "class-validator";
import { RoleEnum } from "../../role/role.enum";

export class UpdateUserDto extends PartialType(
    OmitType(CreateUserDto, ['password'])
) {
    @IsArray()
    @IsNumber()
    @IsOptional()
    tasks?: number[];

    @IsIn(["Admin", "Manager", "Employee"])
    @IsOptional()
    @IsNumber()
    role_id?: RoleEnum;

    @IsArray()
    @IsNumber()
    @IsOptional()
    projects?: number[];
}