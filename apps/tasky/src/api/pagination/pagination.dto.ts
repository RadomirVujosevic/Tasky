import { Contains, IsNumberString, IsOptional, isPositive, Min, NotContains } from "class-validator";


export class PaginationDTO {

    @IsNumberString()
    @IsOptional()
    //negative and decimal numbers are not allowed
    @NotContains("-")
    @NotContains(".")
    limit: number;

    @IsNumberString()
    @IsOptional()
    @NotContains("-")
    @NotContains(".")
    page: number;
}