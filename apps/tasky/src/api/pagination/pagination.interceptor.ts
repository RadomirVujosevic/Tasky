import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { map, Observable } from "rxjs";
import { PaginationDTO } from "./pagination.dto";

@Injectable()
export class PaginationInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {

        return next.handle()
            .pipe(map((data: []) => {
                const query = context.switchToHttp().getRequest().query;
                const limit = +query.limit || 20;
                const page = +query.page || 1;

                if (limit > data.length) {
                    return data;
                }

                if (page * limit > data.length) {
                    //if the query requests more items than are available, return the last <limit> items
                    return data.slice(data.length - limit)
                }

                //page numbers start at 1, but arrays are 0-indexed
                return data.slice((page - 1) * limit, page * limit);
            }))
    }
}