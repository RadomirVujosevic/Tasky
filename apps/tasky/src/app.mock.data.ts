import { getMockProjects } from "./project/project.mock.data";
import { getMockRoles } from "./role/role.mock.data"
import { getMockTasks } from "./task/task.mock.data";
import { getMockUsers } from "./users/users.mock.data";



export const getMockData = () => {
    const roles = getMockRoles();

    const users = getMockUsers(roles[0], roles[1], roles[2]);
    const projects = getMockProjects(users[0]);
    users[1].worksOn = [projects[0], projects[1]];
    users[2].worksOn = [projects[0], projects[1]];
    const tasks = getMockTasks(users[0], users[1], users[0], projects[0], projects[1]);

    return {
        roles: roles,
        users: users,
        projects: projects,
        tasks: tasks,
    }

}